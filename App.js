/* global console */
import React from 'react';
import { WebBrowser, Constants, Linking, SecureStore, Notifications, Permissions } from 'expo';
import { StyleSheet, Text, View, ScrollView } from 'react-native';
import { Button, Header, Overlay, Icon, ListItem } from 'react-native-elements';
// import Buffer from 'buffer';
import { EVE_LOGIN_URL } from './src/consts';
import eve_api from './src/eve_api';
import QueryToObject from './src/query_to_object';

class App extends React.Component {
  constructor(props) {
    super(props);

    this.startLogin = this.startLogin.bind(this);
    this.loadSavedData = this.loadSavedData.bind(this);
    this.downloadData = this.downloadData.bind(this);
    this.scheduleIndustryNotifications = this.scheduleIndustryNotifications.bind(this);

    this.state = {
      is_authenticating: false,
      response: null,
      redirect_data: null,
      oauth: null,
    };
  }

  componentDidMount() {
    this.loadSavedData();
  }

  async loadSavedData() {
    const oauth = await SecureStore.getItemAsync('oauth');
    if (!oauth) return;

    this.setState(
      { oauth: JSON.parse(oauth) },
      () => this.downloadData(true)
    );
  }

  // Download Verify and Industry
  async downloadData(retry = true) {
    const { oauth } = this.state;
    console.log('oauth stored data:', oauth);
    try {
      const verify = await eve_api.verify(oauth.access_token);
      console.log(verify);
      this.setState({ verify });
      const character_id = verify.CharacterID;

      // Industry
      const industry_jobs = await eve_api.industry_jobs(oauth.access_token, character_id);
      this.setState({ industry_jobs }, this.scheduleIndustryNotifications);
    } catch (err_response) {
      console.log('something went wrong.', err_response.status);
      if (retry && err_response.status != 404) {
        console.log('retrying...');
        console.log(oauth.refresh_token);
        const oauth_json = await eve_api.refresh_token(oauth.refresh_token);
        console.log('refreshed oauth data', oauth_json);
        SecureStore.setItemAsync('oauth', JSON.stringify(oauth_json));
        this.setState({ oauth_json }, () => this.downloadData(false));
      } else {
        this.setState({ error: 'cannot retrieve data.' });
      }
    }
  }

  async startLogin() {
    const response = await WebBrowser.openAuthSessionAsync(EVE_LOGIN_URL);

    this.setState({
      response,
      is_authenticating: true,
    });

    if (!response || response.type !== 'success' || !response.url) {
      this.setState({
        is_authenticating: false,
        error: 'invalid response',
      });
      return;
    }

    const url = response.url;
    const query = url.substr(url.indexOf('?') + 1);
    const responseData = QueryToObject(query);

    this.setState({ responseData });

    try {
      const oauth_json = await eve_api.authenticate_token(responseData.code);

      console.log('received oauth data:', oauth_json);
      SecureStore.setItemAsync('oauth', JSON.stringify(oauth_json));
      this.setState(
        { oauth: oauth_json },
        () => this.downloadData(true)
      );
    } catch (err) {
      console.error(err);
      this.setState({ error: err });
    }

    this.setState({ is_authenticating: false });
  }

  async scheduleIndustryNotifications() {
    const result = await Permissions.askAsync(Permissions.NOTIFICATIONS);
    if (result.status !== 'granted' && !result.allowsAlert) {
      console.log('no notification permissions', result);
      return;
    }

    console.log('scheduleIndustryNotifications');
    const { industry_jobs } = this.state;
    // Industry Notifications
    // const cancelResponse = await Notifications.cancelAllScheduledNotificationsAsync();
    // console.log('cancelall notifications', cancelResponse);
    if (!industry_jobs) return;

    for (let i = 0; i < industry_jobs.length; i++) {
      const job = industry_jobs[i];
      const localNotification = {
        title: 'Industry Job Completed',
        body: `Industry job completed: ${job.blueprint_type_id}`,
      };
      const complete_time = new Date(job.end_date);
      const time_left = complete_time.getTime() - (new Date()).getTime();
      const completed = time_left < 0;

      if (!completed) {
        const schedulingOptions = { time: complete_time.getTime() };
        await Notifications.scheduleLocalNotificationAsync(localNotification, schedulingOptions);
      }
    }
  }

  render() {
    const CharacterInfo = this.state.verify ? (
      <Text>{this.state.verify.CharacterName} [{this.state.verify.CharacterID}]</Text>
    ) : null;
    const CharacterIndustry = this.state.industry_jobs && this.state.industry_jobs.length > 0
    ? (
      <View>
        {this.state.industry_jobs.map(entry => {
          const complete_time = new Date(entry.end_date);
          const time_left = complete_time.getTime() - (new Date()).getTime();

          const completed = time_left < 0;

          return (
            <ListItem
              key={`industry-${entry.job_id}`}
              leftAvatar={{ source: { uri: `https://imageserver.eveonline.com/Type/${entry.blueprint_type_id}_64.png` } }}
              title={`item ${entry.blueprint_type_id}`}
              subtitle={completed ? 'done' : `finishes at ${entry.end_date}`}
            />
          );
        })}
      </View>
    )
    : <Text>No Industry Jobs</Text>;

    return (
      <View>
        <Header
          leftComponent={{ icon: 'menu', color: '#fff' }}
          centerComponent={{ text: 'EveOne', style: { color: '#fff' } }}
        />

        <ScrollView>
          {CharacterInfo}
          {CharacterIndustry}
          <Button title="Login with Eve Online" onPress={this.startLogin} />
          {/* <Text>{JSON.stringify(this.state)}</Text>
          <Text>{EVE_LOGIN_URL}</Text>
          <Text>{Constants.linkingUrl}</Text> */}
        </ScrollView>
        <Overlay isVisible={this.state.is_authenticating}>
          <Text>
            Checking credentials...
          </Text>
        </Overlay>
      </View>
    );
  }
}

export default App;

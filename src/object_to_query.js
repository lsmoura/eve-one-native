const ObjectToQuery = (obj) => Object.keys(obj)
.map(option_key => `${option_key}=${encodeURIComponent(obj[option_key])}`)
.join('&');

export default ObjectToQuery;

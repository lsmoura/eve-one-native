/* global require console fetch */
import { CLIENT_ID, CLIENT_SECRET } from './consts';
const Buffer = require('buffer/').Buffer;

import ObjectToQuery from './object_to_query';

const API_HOSTNAME = 'https://esi.evetech.net/latest';
const API_VERIFY_HOSTNAME = 'https://esi.tech.ccp.is';
const OAUTH_TOKEN_URL = 'https://login.eveonline.com/oauth/token';

const api_request_get = async (endpoint, access_token) => {
  const endpoint_prefix = endpoint.indexOf('verify') > 0 ? API_VERIFY_HOSTNAME : API_HOSTNAME;
  const url = `${endpoint_prefix}${endpoint}`;
  const options = {
    method: 'GET',
    headers: {
      authorization: `Bearer ${access_token}`,
    },
  };

  console.log('fetch', url, options);

  const response = await fetch(url, options);

  if (response.status < 200 || response.status >= 400) {
    throw response;
  }

  return response.json();
};

const eve_api = {
  verify: (access_token) => {
    if (!access_token) {
      return Promise.reject(new Error('verify(): no access_token given'));
    }

    return api_request_get('/verify', access_token);
  },
  industry_jobs: (access_token, character_id) => {
    if (!access_token) {
      return Promise.reject(new Error('industry_jobs(): no access_token given'));
    }
    if (!character_id) {
      return Promise.reject(new Error('industry_jobs(): no character_id given'));
    }

    return api_request_get(`/characters/${character_id}/industry/jobs/`, access_token);
  },
  refresh_token: async (refresh_token) => {
    if (!refresh_token) {
      return Promise.reject(new Error('refresh_token(): no refresh_token given'));
    }

    const post_body = {
      grant_type: 'refresh_token',
      refresh_token: refresh_token,
    };

    const fetch_options = {
      method: 'POST',
      headers: {
        authorization: `Basic ${Buffer.from([CLIENT_ID, CLIENT_SECRET].join(':')).toString('base64')}`,
        'content-type': 'application/x-www-form-urlencoded',
      },
      body: ObjectToQuery(post_body),
      cache: 'no-cache',
    };
    console.log('starting request', OAUTH_TOKEN_URL, fetch_options);
    const oauth_response = await fetch(OAUTH_TOKEN_URL, fetch_options);

    console.log('oauth response', oauth_response);
    if (oauth_response.status < 200 || oauth_response >= 300) {
      throw oauth_response;
    }

    return oauth_response.json();
  },
  authenticate_token: async (code) => {
    const post_body = {
      grant_type: 'authorization_code',
      code: code,
    };

    const fetch_options = {
      method: 'POST',
      headers: {
        authorization: `Basic ${Buffer.from([CLIENT_ID, CLIENT_SECRET].join(':')).toString('base64')}`,
        'content-type': 'application/x-www-form-urlencoded',
      },
      body: ObjectToQuery(post_body),
      cache: 'no-cache',
    };
    console.log('starting request', OAUTH_TOKEN_URL, fetch_options);
    const oauth_response = await fetch(OAUTH_TOKEN_URL, fetch_options);

    console.log(oauth_response);
    if (oauth_response.status < 200 || oauth_response >= 300) {
      throw oauth_response;
    }

    return oauth_response.json();
  },
};

export default eve_api;

const QueryToObject = (query) => query.split('&')
.reduce(
  (response, piece) => {
    const pos = piece.indexOf('=');
    if (pos < 0) {
      response[piece] = true;
    } else {
      response[piece.substr(0, pos)] = decodeURIComponent(piece.substr(pos + 1));
    }

    return response;
  },
  {}
);

export default QueryToObject;

/* global process */
import {
  CLIENT_ID as _CLIENT_ID,
  CLIENT_SECRET as _CLIENT_SECRET,
  CALLBACK_URL as _CALLBACK_URL,
} from 'react-native-dotenv';
import ObjectToQuery from './object_to_query';

export const CLIENT_ID = _CLIENT_ID;
export const CLIENT_SECRET = _CLIENT_SECRET;
export const CALLBACK_URL = _CALLBACK_URL;

export const APP_SCOPES = [
  'publicData',
  'esi-location.read_location.v1',
  'esi-location.read_ship_type.v1',
  'esi-wallet.read_character_wallet.v1',
  'esi-assets.read_assets.v1',
  'esi-planets.manage_planets.v1',
  'esi-industry.read_character_jobs.v1',
  'esi-markets.read_character_orders.v1',
  'esi-characters.read_blueprints.v1',
  'esi-location.read_online.v1',
  'esi-industry.read_character_mining.v1',
];

const LOGIN_STATE_PREFIX = process.env.NODE_ENV.substr(0, 3);

export const EVE_OPTIONS = {
  response_type: 'code',
  redirect_uri: CALLBACK_URL,
  client_id: CLIENT_ID,
  scope: APP_SCOPES.join(' '),
  state: `${LOGIN_STATE_PREFIX}/${Math.random().toString(36).substring(7)}`,
};


const eve_query = ObjectToQuery(EVE_OPTIONS);
export const EVE_LOGIN_URL = `https://login.eveonline.com/oauth/authorize/?${eve_query}`;
